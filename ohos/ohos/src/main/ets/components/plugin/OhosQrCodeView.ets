/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { scanBarcode, customScan } from '@kit.ScanKit';
import { common, abilityAccessCtrl, Permissions } from '@kit.AbilityKit';
import { Params } from '@ohos/flutter_ohos/src/main/ets/plugin/platform/PlatformView';
import { MethodResult } from '@ohos/flutter_ohos';
import { BusinessError } from '@kit.BasicServicesKit';
import { PermissionsUtil } from './libs/PermissionsUtil';
import QrCaptureView from './QrCaptureView';
import EventHub from './libs/EventHub';

const TAG: string = '[OhosView]: '

@Component
export struct OhosView {
  @Prop params: Params;
  @State isFlashlight: boolean = false; // Whether the flashlight is turned on.
  @State isSensorLight: boolean = false; // Record the current environment brightness status.
  @State isScanned: boolean = false;
  @State cameraHeight: number = 300; // Sets the XComponent height. The default unit is vp.
  @State cameraWidth: number = 300; // Sets the XComponent width. The default unit is vp.
  @State userGrant: boolean = false;
  @State surfaceId: string = '';
  @State cameraScreenOffsetX: number = 0;
  @State cameraScreenOffsetY: number = 0;
  @State isStop: boolean = false;
  @State result: Array<scanBarcode.ScanResult> = [];
  @State data: Array<string> = [];
  @State isVertical: boolean = true;
  private mXComponentController: XComponentController = new XComponentController();
  private viewControl: customScan.ViewControl = { width: 1920, height: 1080, surfaceId: this.surfaceId };

  async aboutToAppear() {
    console.info(TAG + 'OhosView page aboutToAppear success')
    this.isScanned = false;
    this.isStop = false;
    this.init();
    this.onEmitEvents()
  }

  aboutToDisappear() {
    console.info(TAG + 'OhosView page aboutToDisappear success')
  }

  onEmitEvents() {
    // 注册监听事件
    const context = getContext(this) as common.UIAbilityContext;
    // 触发检查权限
    EventHub.onCheckAndRequestPermission(context, (result: MethodResult) => {
      console.info(TAG + 'onCheckAndRequestPermission enter success');
      this.checkPermission(result);
    });
    // 触发重新扫描
    EventHub.onResum(context, (params: string) => {
      console.info(TAG + 'onResum enter success');
      this.restartCustomScan();
    });
    // 触发暂停扫描
    EventHub.onPause(context, () => {
      console.info(TAG + 'onPause enter success');
      this.stopCustomScan();
    });
    // 设置手电筒开关
    EventHub.onSetTorch(context, (status: boolean) => {
      this.setTorchMode(status);
      console.info(TAG + 'onSetTorch success');
    });
    // 从相册获取图片解码
    EventHub.onDecode(context, async (data: string[]) => {
      this.data = data;
      setTimeout(() => {
        this.restartCustomScan('decode');
      })
    });
    // 获取扫码结果
    EventHub.onResult(context, async (result: MethodResult) => {
      result.success({
        data: this.data || [],
        result: this.result || []
      })
    });
    // 返回后销毁监听事件
    EventHub.onDispose(context, async () => {
      if (!this.isStop) {
        customScan.closeFlashLight()
        await customScan.stop();
        await customScan.release();
        this.isStop = true
      }
      this.isScanned = false;
      EventHub.offAll(context);
      console.info(TAG + 'onDispose success');
    });
  }

  // 检查权限 没有的话申请权限
  async checkPermission(result: MethodResult) {
    const permissions: Array<Permissions> = ['ohos.permission.CAMERA'];
    let grantStatus = await PermissionsUtil.checkAccessToken(permissions[0]);
    if (grantStatus === abilityAccessCtrl.GrantStatus.PERMISSION_GRANTED) {
      this.userGrant = true;
    } else {
      this.requestCameraPermission(() => {
      }, result);
    }
  }

  // 检查权限 初始化相机 开始扫描
  async init() {
    const permissions: Array<Permissions> = ['ohos.permission.CAMERA'];
    let grantStatus = await PermissionsUtil.checkAccessToken(permissions[0]);
    if (grantStatus === abilityAccessCtrl.GrantStatus.PERMISSION_GRANTED) {
      this.userGrant = true;
      try {
        customScan.init();
      } catch (error) {
        console.error(TAG + 'customScan.init() api error:' + JSON.stringify(error));
      }
      if (this.surfaceId) {
        this.startCustomScan();
      }
    } else {
      this.requestCameraPermission(() => {
        customScan.init();
      });
    }
    this.result = [];
    this.data = [];
  }

  // 申请权限
  async requestCameraPermission(callBack: () => void = () => {
  }, result: MethodResult | null = null) {
    const context = getContext(this);
    let grantStatus = await PermissionsUtil.reqPermissionsFromUser(context);
    let length: number = grantStatus.length;
    for (let i = 0; i < length; i++) {
      if (grantStatus[i] === 0) {
        this.userGrant = true;
        try {
          callBack && callBack();
        } catch (error) {
          console.error(TAG + ' requestCameraPermission err' + JSON.stringify(error));
        }
      } else {
        this.userGrant = false;
        result?.error("cameraPermission", "MediaRecorderCamera permission not granted", null);
      }
    }
  }

  // 重新扫描
  restartCustomScan(from: string = '') {
    if (from !== 'decode') {
      this.isScanned = false;
    }
    if (this.isStop) {
      this.startCustomScan();
    } else {
      customScan.stop().then(() => {
        this.startCustomScan();
      }).catch((err: BusinessError) => {
        console.error(TAG + ' restartCustomScan stop err' + JSON.stringify(err));
      })
    }
  }

  // 开始扫描
  startCustomScan() {
    this.viewControl = { width: this.cameraWidth, height: this.cameraHeight, surfaceId: this.surfaceId };
    try {
      customScan.start(this.viewControl).then(async (result: Array<scanBarcode.ScanResult>) => {
        this.callback(result);
      }).catch((err: BusinessError) => {
        console.error(TAG + " startCustomScan customScan.start err:" + JSON.stringify(err));
      })
      this.isStop = false;
      if (this.isFlashlight) {
        customScan.openFlashLight();
      }
      customScan.on('lightingFlash', (error, isLightingFlash) => {
        if (error) {
          return;
        }
        this.isSensorLight = isLightingFlash;
      });
      console.info(TAG + " startCustomScan  success");
    } catch (err) {
      console.error(TAG + " startCustomScan  err:" + JSON.stringify(err));
    }
  }

  callback(result: Array<scanBarcode.ScanResult>) {
    // 需要先暂停后才能继续识别
    customScan.stop().then(() => {
      this.isScanned = true;
      this.result = result;
      this.data = this.result.map(el => el.originalValue);
      // 连续识别
      this.startCustomScan()
    })
  }

  // 暂停扫描:如果手电筒开始的, 点击暂停后- 手电筒关闭- 重新点击重启: 手电筒保持暂停前状态
  async stopCustomScan(): Promise<void> {
    try {
      if (!this.isStop) {
        customScan.closeFlashLight()
        await customScan.stop();
        await customScan.release();
        this.isStop = true
      }
      console.info(TAG + " stopCustomScan  success");
    } catch (e) {
      console.error(TAG + " stopCustomScan  err" + JSON.stringify(e));
    }
  }

  // 设置手电筒开关
  setTorchMode(status: boolean) {
    try {
      if (!this.isStop) {
        const light = customScan.getFlashLightStatus();
        if (light) {
          customScan.closeFlashLight()
          this.isFlashlight = false;
        } else {
          customScan.openFlashLight();
          this.isFlashlight = true;
        }
      }
      console.info(TAG + " setTorchMode  success");
    } catch (e) {
      console.error(TAG + " setTorchMode  err" + JSON.stringify(e));
    }
  }

  build() {
    Stack() {
      if (this.userGrant) {
        Column() {
          XComponent({
            id: 'componentId',
            type: 'surface',
            controller: this.mXComponentController
          })
            .onLoad(() => {
              this.surfaceId = this.mXComponentController.getXComponentSurfaceId();
              this.startCustomScan();
            })
            .width(this.cameraWidth)
            .height(this.cameraHeight)
            .position({ x: this.cameraScreenOffsetX, y: this.cameraScreenOffsetY })
        }
        .height('100%')
        .width('100%')
        .position({ x: 0, y: 0 })
      }
      if (this.isScanned) {
        if (this.data?.length) {
          Text(this.data.join(','))
        }
      }
    }
    .width('100%')
    .height('100%')
    .backgroundColor(this.userGrant ? Color.Transparent : Color.Black)
  }
}

@Builder
export function buildOhosView(params: Params) {
  OhosView({
    params: params
  })
}